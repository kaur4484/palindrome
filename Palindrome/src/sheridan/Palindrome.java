
package sheridan;

public class Palindrome {
	
	
	public static boolean isPalindrome( String input) {
		
		input = input.toLowerCase().replaceAll(" "," ");
		
		for (int i = 0 , j = input.length() - 1 ; i < j ; i++, j--) {
			if ( input.charAt(i) != input.charAt(j)) {
				return false;				
			}
		}
		return true;
	}
	
	public static void main(String[] args) {
		System.out.println("is ANNA a palindrome? " + Palindrome.isPalindrome("anna"));
		System.out.println("is RAJ a palindrome? " + Palindrome.isPalindrome("raj"));
		System.out.println("is MADAM palindrome? " + Palindrome.isPalindrome("madam"));
		System.out.println("is IQBAL palindrome? " + Palindrome.isPalindrome("iqbal"));
		
	}
	

}
